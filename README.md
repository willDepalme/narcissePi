# NarcissePi

A smart mirror project on RaspberryPi

## Installation

This project is meant to be run on a web browser on fullscreen mode (no border, no header).

Clone the repo and launch your web browser from the root folder in the following way.

Google Chrome
`google-chrome --start-fullscreen index.html`

## Customisation


## Contribute

You can create your own custom modules for your mirror.

Create a new folder in the modules directory. Then create a js file with the same name as your folder.

Create your own class that must extends the Module class.

`class Hello extends Module {}`

In your constructor, initialize the configuration of your module (refresh rate, folder name, ...)

```
constructor(){
		super();

		this.intervalRate = 5000;
		this.folder = "hello";
}
```

Implements the methods `generateDom`, `update` and `display`.

In the index.html, add a script to link your module.

`<script type="text/javascript" src="modules/hello/hello.js"></script>`

Then create a new instance of your module in the `window.onload()` function and add it to the modules list.

```
let hello = new Hello();

let modules = [clock, hello, weather];

```