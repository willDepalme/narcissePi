/**
* Clock module
*/
class Clock extends Module{

	
	constructor(){
		super();

		this.intervalRate = 1000;
		this.folder = "clock";
		
		this.days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Saturday"];
		this.months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

		this.time = "";
		this.date = "";

		this.generateDom();
	}

	generateDom(){
		this.linkStylesheet("clock.css");

		var modules = document.getElementById("modules");

		if(modules){
			/*
			<section id="clock" class="module">
				<div id="clock-time"></div>
				<div id="clock-date"></div>
			</section> 
			*/

			var clockDom = document.createElement("section");
			clockDom.className = "module";
			clockDom.id = "clock";

			var clockTimeDom = document.createElement("div");
			clockTimeDom.id = "clock-time";

			var clockDateDom = document.createElement("div");
			clockDateDom.id = "clock-date";

			clockDom.appendChild(clockTimeDom);
			clockDom.appendChild(clockDateDom);
			modules.appendChild(clockDom);
		}
	}

	update(){
		var d = new Date();
		this.time = d.getMinutes() > 9 ? d.getHours() + ':' + d.getMinutes() : d.getHours() + ':0' + d.getMinutes();
		this.date = this.days[d.getDay()] + ' ' + d.getDate() + ' ' + this.months[d.getMonth()];
	}

	display(){
		document.getElementById("clock-time").innerHTML = this.time;
		document.getElementById("clock-date").innerHTML = this.date;
	}
}

