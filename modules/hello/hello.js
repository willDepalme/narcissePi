/**
* Simple module to say hello
*/
class Hello extends Module{

	constructor(){
		super();

		this.intervalRate = 5000;
		this.folder = "hello";

		this.messages = [
			"Hello beautiful",
			"Hi ! You look nice today",
			"Hello cutie !",
			"Nice look buddy !",
			"You're wonderful, as always"
		];

		this.generateDom();
	}

	generateDom(){
		this.linkStylesheet("hello.css");

		var modules = document.getElementById("modules");

		if(modules){
			/*
			<section id="hello" class="module">
				<div id="hello-message"></div>
			</setion>
			*/
			var helloDom = document.createElement("section");
			helloDom.id = "hello";
			helloDom.className = "module";

			var helloMessageDom = document.createElement("div");
			helloMessageDom.id = "hello-message";

			helloDom.appendChild(helloMessageDom);
			modules.appendChild(helloDom);
		}
	}

	update(){
		var index = this.getRandomInt(this.messages.length);
		this.currentMessage = this.messages[index];
	}

	display(){
		var messageDom = document.getElementById("hello-message");
		if(messageDom){
			messageDom.innerHTML = this.currentMessage;
		}
	}

	getRandomInt(max) {
	  return Math.floor(Math.random() * Math.floor(max));
	}
}