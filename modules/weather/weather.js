/**
* Weather module
*
*/
class Weather extends Module{

	constructor(){
		super();

		this.intervalRate = 60000;
		this.folder = "weather";
		
		this.generateDom();
	}

	generateDom(){
		this.linkStylesheet("weather.css");

		var modules = document.getElementById("modules");

		if(modules){
			/*
			<section id="weather" class="module">
				<div class="left">
					<div id="weather-temp"></div>
					<div id="weather-city"></div>
				</div>
				<div class="right">
					<img id="weather-icon" src="modules/weather/assets/x.svg">
				</div>

				<div id="weather-days">
					<div id="weather-day-1" class="weather-day">
						<div class="icon"></div>
						<div class="day">26</div>
						<div class="temp"></div>
					</div>
					<div id="weather-day-2" class="weather-day">
						...
					</div>
					...
				</div>
			</section> 
			*/

			var weatherDom = document.createElement("section");
			weatherDom.id = "weather";
			weatherDom.className = "module";

			var  weatherLeftContainerDom = document.createElement("div");
			weatherLeftContainerDom.className = "left";
			var  weatherRightContainerDom = document.createElement("div");
			weatherRightContainerDom.className = "right";

			var weatherTempDom = document.createElement("div");
			weatherTempDom.id = "weather-temp";
			var weatherCityDom = document.createElement("div");
			weatherCityDom.id = "weather-city";
			var weatherIconDom = document.createElement("img");
			weatherIconDom.id = "weather-icon";

			weatherLeftContainerDom.appendChild(weatherTempDom);
			weatherLeftContainerDom.appendChild(weatherCityDom);
			weatherRightContainerDom.appendChild(weatherIconDom);

			weatherDom.appendChild(weatherLeftContainerDom);
			weatherDom.appendChild(weatherRightContainerDom);

			var weatherDaysDom = document.createElement("div");
			weatherDaysDom.id = "weather-days";
			for(let day = 1 ; day <= 3 ; day++){
				var weatherDayDom = document.createElement("div");
				weatherDayDom.className = "weather-day";
				weatherDayDom.id = "weather-day-" + day;

				var weatherDayIconDom = document.createElement("img");
				weatherDayIconDom.id = "weather-day-" + day + "-icon"
				var weatherDayDayDom = document.createElement("div");
				weatherDayDayDom.id = "weather-day-" + day + "-day";
				var weatherDayTempDom = document.createElement("div");
				weatherDayTempDom.id = "weather-day-" + day + "-temp";

				weatherDayDom.appendChild(weatherDayIconDom);
				weatherDayDom.appendChild(weatherDayDayDom);
				weatherDayDom.appendChild(weatherDayTempDom);

				weatherDaysDom.appendChild(weatherDayDom);
			}
			weatherDom.appendChild(weatherDaysDom);

			modules.appendChild(weatherDom);
		}
	}

	update(){
		/*
		{
			"title" : "Lyon",
			"consolidated_weather": {
				0 : {
					applicable_date
					humidity
					max_temp
					min_temp
					predictability
					the_temp
					weather_state_abbr
					weather_state_name
					wind_'direction
					wind_direction_compass
					wind_speed
				}, //Today
				1 : {}, //Tomorrow
				...
			}
		}
		*/

		// https://www.metaweather.com/api/location/609125/

		this.data = {"consolidated_weather":[{"id":4852735249743872,"weather_state_name":"Light Cloud","weather_state_abbr":"lc","wind_direction_compass":"N","created":"2018-07-25T10:08:52.643200Z","applicable_date":"2018-07-25","min_temp":20.136666666666667,"max_temp":33.033333333333339,"the_temp":33.144999999999996,"wind_speed":4.4553747669450408,"wind_direction":354.5,"air_pressure":999.255,"humidity":44,"visibility":12.959006899705718,"predictability":70},{"id":4664519179632640,"weather_state_name":"Clear","weather_state_abbr":"c","wind_direction_compass":"N","created":"2018-07-25T10:08:56.355470Z","applicable_date":"2018-07-26","min_temp":20.0,"max_temp":33.266666666666673,"the_temp":32.619999999999997,"wind_speed":3.1509007885097695,"wind_direction":352.20551889617809,"air_pressure":997.245,"humidity":43,"visibility":13.989861707627455,"predictability":68},{"id":4644667949842432,"weather_state_name":"Light Cloud","weather_state_abbr":"lc","wind_direction_compass":"N","created":"2018-07-25T10:08:58.837390Z","applicable_date":"2018-07-27","min_temp":19.643333333333331,"max_temp":34.346666666666671,"the_temp":33.740000000000002,"wind_speed":1.8540483661125693,"wind_direction":358.00825832350506,"air_pressure":995.98500000000001,"humidity":39,"visibility":14.754769645271613,"predictability":70},{"id":4927442422595584,"weather_state_name":"Showers","weather_state_abbr":"s","wind_direction_compass":"NW","created":"2018-07-25T10:09:02.068370Z","applicable_date":"2018-07-28","min_temp":19.923333333333336,"max_temp":31.143333333333334,"the_temp":27.640000000000001,"wind_speed":3.227902671473263,"wind_direction":309.5,"air_pressure":996.58999999999992,"humidity":66,"visibility":13.020833333333334,"predictability":73},{"id":5770567856685056,"weather_state_name":"Heavy Cloud","weather_state_abbr":"hc","wind_direction_compass":"N","created":"2018-07-25T10:09:05.623390Z","applicable_date":"2018-07-29","min_temp":17.903333333333332,"max_temp":28.013333333333332,"the_temp":27.280000000000001,"wind_speed":4.9025216259100191,"wind_direction":354.0,"air_pressure":996.88499999999999,"humidity":53,"visibility":14.692321840451761,"predictability":71},{"id":6495905851113472,"weather_state_name":"Clear","weather_state_abbr":"c","wind_direction_compass":"N","created":"2018-07-25T10:09:08.048090Z","applicable_date":"2018-07-30","min_temp":18.126666666666665,"max_temp":30.943333333333332,"the_temp":27.899999999999999,"wind_speed":3.3423069275431478,"wind_direction":7.4999999999999991,"air_pressure":979.89999999999998,"humidity":51,"visibility":9.9978624830987037,"predictability":68}],"time":"2018-07-25T13:21:34.383320+02:00","sun_rise":"2018-07-25T06:15:37.531461+02:00","sun_set":"2018-07-25T21:18:09.020273+02:00","timezone_name":"LMT","parent":{"title":"France","location_type":"Country","woeid":23424819,"latt_long":"46.71,1.72"},"sources":[{"title":"BBC","slug":"bbc","url":"http://www.bbc.co.uk/weather/","crawl_rate":180},{"title":"Forecast.io","slug":"forecast-io","url":"http://forecast.io/","crawl_rate":480},{"title":"HAMweather","slug":"hamweather","url":"http://www.hamweather.com/","crawl_rate":360},{"title":"Met Office","slug":"met-office","url":"http://www.metoffice.gov.uk/","crawl_rate":180},{"title":"OpenWeatherMap","slug":"openweathermap","url":"http://openweathermap.org/","crawl_rate":360},{"title":"World Weather Online","slug":"world-weather-online","url":"http://www.worldweatheronline.com/","crawl_rate":360},{"title":"Yahoo","slug":"yahoo","url":"http://weather.yahoo.com/","crawl_rate":180}],"title":"Lyon","location_type":"City","woeid":609125,"latt_long":"45.759392,4.828980","timezone":"Europe/Paris"};
	}

	display(){

		var weatherTempDom = document.getElementById("weather-temp");
		if(weatherTempDom){
			weatherTempDom.innerHTML = parseInt(this.data['consolidated_weather'][0]['the_temp']) + '°C';
		}

		var weatherCityDom = document.getElementById("weather-city");
		if(weatherCityDom){
			weatherCityDom.innerHTML = this.data['title'];
		}

		var weatherIconDom = document.getElementById("weather-icon");
		if(weatherIconDom){
			weatherIconDom.src = "modules/weather/assets/images/" + this.data['consolidated_weather'][0]['weather_state_abbr'] + ".svg";
		}

		for(let day = 1 ; day <= 3 ; day++){
			var weatherDayDayDom = document.getElementById("weather-day-" + day + "-day");
			if(weatherDayDayDom){
				var dayDate = new Date(this.data['consolidated_weather'][day]['applicable_date']);
				weatherDayDayDom.innerHTML = dayDate.getUTCDate() + "-" + parseInt(dayDate.getMonth()+1);
			}
			var weatherDayTempDom = document.getElementById("weather-day-" + day + "-temp");
			if(weatherDayTempDom){
				weatherDayTempDom.innerHTML = parseInt(this.data['consolidated_weather'][day]['the_temp']) + '°C';
			}
			var weatherDayIconDom = document.getElementById("weather-day-" + day + "-icon");
			if(weatherDayIconDom){
				weatherDayIconDom.src = "modules/weather/assets/images/" + this.data['consolidated_weather'][day]['weather_state_abbr'] + ".svg";
			}
		}

	}
}