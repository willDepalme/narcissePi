//                        _              _____ _ 
//                       (_)            |  __ (_)
//   _ __   __ _ _ __ ___ _ ___ ___  ___| |__) | 
//  | '_ \ / _` | '__/ __| / __/ __|/ _ \  ___/ |
//  | | | | (_| | | | (__| \__ \__ \  __/ |   | |
//  |_| |_|\__,_|_|  \___|_|___/___/\___|_|   |_|
//
//  Your simple web based smart mirror


/**
* Main class of the application
*/
class Narcisse{

	constructor(modules){
		this.modules = modules;
	}

	run(){
		for(let i in this.modules){
			let module = this.modules[i];
			module.update();
			module.display();
			module.run();
		}
	}

	stop(){
		for(let i in this.modules){
			let module = this.modules[i];
			module.stop();
		}
	}
}

class Module{

	constructor(){
		this.intervalRate = 1000;
		this.folder = "";
	}

	generateDom(){}

	linkStylesheet(cssUrl){
		var head  = document.getElementsByTagName('head')[0];
	    var link  = document.createElement('link');
	    link.rel  = 'stylesheet';
	    link.type = 'text/css';
	    link.href = 'modules/' + this.folder + '/' + cssUrl;
	    link.media = 'all';
	    head.appendChild(link);
	}

	update(){}

	display(){}

	run(){
		let self = this;

		this.interval = setInterval(
			function(){
				self.update();
				self.display();
			}, 
			this.intervalRate
		);
	}

	stop(){
		this.interval = null;
	}

	setRefreshRate(rate){
		this.intervalRate = rate;
	}

	getRefreshRate(){
		return this.intervalRate;
	}
}


////////////////////////////////////////////////////////////////////


class Ajax{

	static get(url, callback){
		var xhttp = new XMLHttpRequest();
	  	xhttp.onreadystatechange = function() {
	     	callback();
	  	};
	  	xhttp.open("GET", url, true);
	  	xhttp.send();
	}

}